# Relativity and the 'Physics' of Love
_by Albert Einstein, interpretation by Michael R. Burch_

>   Sit next to a pretty girl for an hour,
>   it seems like a minute.
>   Sit on a red-hot stove for a minute,
>   it seems like an hour.
>   That's relativity!

>   Oh, it should be possible
>   to explain the laws of physics
>   to a barmaid!...
>   but how could she ever,
>   in a million years,
>   explain love to an Einstein?

>   All these primary impulses,
>   not easily described in words,
>   are the springboards
>   of man's actions—because
>   any man who can drive safely
>   while kissing a pretty girl
>   is simply not giving the kiss
>   the attention it deserves!

Submitted by Jiajun Chen
