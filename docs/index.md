# The AION Poetry Competition

Welcome to the AION poetry competition. 

Here, we take a break from our rigourous pursuit of science to appreciate the beautiful things in life. You can browse the list of people's favourite poems below for inspiration. When you're ready to submit your own, try [a GitLab tutorial](https://www.w3schools.com/git/git_getstarted.asp?remote=gitlab) and learn how to branch, merge and unit test by adding your poem to https://gitlab.com/charlesbaynham/poetry-competition. 

## Poems

* [Charles Baynham - Leisure, by W. H. Davies](charles.md)
* [Sam Hope-Evans - Eternity, by William Blake](sam.md)
* [Richard Hobson - Charles <3 ChatGPT xxo, by ChatGPT](richard.md)
* [Alice Josset - Nuit Rhenane, by Guillaume Apollinaire](alice.md)
* [Marko Wojtkowiak - Howl, by Allen Ginsburg](marko.md)
* [Kamran Hussain - Going Down Hill on a Bicycle:A Boy's Song, by Henry Charles Beeching](kamran.md)
* [Soumyodeep - Quantum Optics, by chatGPT](soumyodeep.md)
* [Upsana - The road, By Robert Frost](up.md)
* [Yogi - Nature, By William Wordsworth](yogi.md)
* [Jiajun Chen - Relativity and the 'Physics' of Love, by by Albert Einstein, interpretation by Michael R. Burch](Jiajun.md)
* [Archie and Bethan - The Raven by Edgar Allan Poe](archie_and_bethan.md)
* [Archie and Bethan - Wild Geese by Mary Oliver](archie_and_bethan2.md)
* [Michael - Nature](mike.md)
* [Bhavs and Mariam - The MOT](bhavmay.md)
* [Mike and Julian](mike_and_julian.md)
* [Chen Lu](chen.md)
* [Leonie and Kanika](leonie_and_kanika.md)
* [Thomas Hird](hird.md)
* [Jesse Schelfhout](jesse.md)
