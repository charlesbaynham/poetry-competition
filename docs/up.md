# The road 
_By Robert Frost_

>   Two roads diverged in yellow wood
>   And sorry I could not travel both 
>   And be one traveller, long I stood
>   To where it bent in the undergrowth
>   
>   Then took the other, as just as fair
>   and having perhaps the better claim
>   Because itv was grassy and wanted wear
>   Though as for that the passing there
>   Had worn them really about the same, 

Submitted by Upasna 