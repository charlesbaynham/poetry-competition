# The AION Poetry Competition

Learn how to use Git to work collaboratively on code by submitting your poem to
the AION poetry competition. You can see the entries so far at
https://charlesbaynham.gitlab.io/poetry-competition/.
